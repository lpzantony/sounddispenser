# SoundDispenser

This repo contains source code for a funny little side project.
It is basically a sound box with a few categories.

# Setup the Raspberry Pi

- install headless raspbian
- setup ssh
- update packages
- install pip3
- install git
- instal pipenv using pip3
- install simpleaudio using pip3
- install gpiozero using pip3
- if you want to test you connections use gpiotest (https://github.com/kgbplus/gpiotest/blob/master/gpiotest.py) (remember to install RPi.gpio with pip3 before using it)

# Improve boottime

The initial boot time (time between powering up the board and earing the program start) was about 1 min 45 sec.

We could reduce to it to (aprox.) 18 sec by doing those things, see below.

## 1 - Editing the `/boot/config.txt` with the following changes:
```
# Disable the rainbow splash screen
disable_splash=1

# Disable bluetooth
dtoverlay=pi3-disable-bt

#Disable Wifi
dtoverlay=pi3-disable-wifi

# Overclock the SD Card from 50 to 100MHz
# This can only be done with at least a UHS Class 1 card
dtoverlay=sdtweak,overclock_50=100

# Set the bootloader delay to 0 seconds. The default is 1s if not specified.
boot_delay=0

# Overclock the raspberry pi. This voids its warranty. Make sure you have a good power supply.
force_turbo=1
```

## 2 - Make the kernel output less verbose by adding the "quiet" flag to the kernel command line in file `/boot/cmdline.txt`

(Notice that we only added the word `quiet`at the end of this line)
```
dwc_otg.lpm_enable=0 console=serial0,115200 console=tty1 root=PARTUUID=32e07f87-02 rootfstype=ext4 elevator=deadline fsck.repair=yes quiet rootwait
```

## 3 - Add a service that runs the code you would like to run as fast as possible (`/lib/systemd/system/sounddispenser.service`):
```
 [Unit]
 Description=The python program for the SoundDispenser game
 DefaultDependencies=no

 [Service]
 Type=idle
 ExecStart=/usr/bin/python3 /home/pi/sounddispenser/sounddispenser/SoundDispenser.py /home/pi/sounddispenser/sounds
 User=pi
 Group=pi
 StandardOutput=syslog
 StandardError=syslog
 SyslogIdentifier=piservice
 Restart=on-failure

 [Install]
 WantedBy=basic.target
```

Then enable it with `sudo systemctl enable sounddispenser.service`

You can try it with `sudo systemctl start sounddispenser.service` and debug it with `sudo systemctl status sounddispenser.service`

# Sources

Fast boot: http://himeshp.blogspot.com/2018/08/fast-boot-with-raspberry-pi.html
Fast boot: https://www.raspberrypi.org/forums/viewtopic.php?t=148414
