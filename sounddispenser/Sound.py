from Player import Player



class Sound:
    """
     Object representation of a Sound.
     This object contains the name of the actual audio file

     Attributes:
         _path (string): The path that lead to the Sound. Can be relative or absolute.
         _name (string): The name of the actual audio file.
     """

    def __init__(self, path: str, name: str):
        """
        Sound's constructor

        Parameters:
            path (string): The complex number to be added.
            name (string): The complex number to be added.
        """
        self._path: str = path
        self._name: str = name

    def getFullPath(self) -> str:
        """
        Gives the path + name as a string. Ex: "../my/music/aSound.mp3"
        Returns:
            str: The sound's path + name as a string.
        """
        return self._path + '/' + self._name

    def getName(self) -> str:
        """
        Gives a copy of this Sounds's name
        Returns:
            str: The sound's name.
        """
        return self._name

    def play(self):
        """
        Plays this Sound
        """
        Player.playFile(self.getFullPath())
