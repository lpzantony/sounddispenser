import os
import threading
from threading import Timer


def isRpi() -> bool:
    return os.uname()[4].startswith("arm")


from Player import Player
from Sound import Sound
from SoundBank import SoundBank
from SoundCategory import SoundCategory
from gpiozero import Button
import sys

if not isRpi():
    from GpioEmulator import GpioEmulator


# from pynput import keyboard


def threadedExecution(func, funcArgs=None):
    if funcArgs is not None:
        threading.Thread(target=lambda x: func(x), args=funcArgs).start()
    else:
        threading.Thread(target=func).start()


if isRpi():
    SPECIALS_PATH = "/home/pi/sounddispenser/sounddispenser/specialSounds"
else:
    SPECIALS_PATH = "sounddispenser/specialSounds"


class MainProcess:
    """
     Object representation of the the main()

     Attributes:
         emu (GpioEmulator): GpioEmulator used when the code is not executed on a Rpi
         blueB (Button): Button instance for the blue button
         whiteB (Button): Button instance for the white button
         rotB (Button): Button instance for the rotary button
         sb : (SoundBank) SoundBank instance
         _timeCounter (int) The counter used for the count of rotary switch's clicks
         _currentCategory (SoundCategory) Currently selected sound category
         _currentSound (Sound) Currently selected sound
         _timer (Timer) : private instance of a Timer used for the count of rotary switch's clicks
         _soundNext (Sound) : Sound played when the game switches to another sound
         _soundStart (Sound) : Sound played at startup
     """
    COUNTER_START: int = 0
    TIMER_TIMEOUT_SECONDS: float = 0.5
    BLUE_BUTTON_GPIO = 20
    WHITE_BUTTON_GPIO = 16
    ROTARY_SWITCH_GPIO = 21

    def __init__(self, mediaFolderPath: str):
        """
        MainProcess's constructor

        Parameters:
            mediaFolderPath (str): Path to the media folder. Used to build a SoundBank instance
        """
        # Use GPIO mocking only on PC
        if not isRpi():
            self.emu: GpioEmulator = GpioEmulator()

        # Init buttons
        self.blueB: Button = Button(MainProcess.BLUE_BUTTON_GPIO, pull_up=False)
        self.whiteB: Button = Button(MainProcess.WHITE_BUTTON_GPIO, pull_up=False, bounce_time=0.5)
        self.rotB: Button = Button(MainProcess.ROTARY_SWITCH_GPIO, pull_up=None, active_state=False)

        # Use GPIO mocking only on PC
        if not isRpi():
            self.emu.start(self.blueB, MainProcess.BLUE_BUTTON_GPIO,
                           self.whiteB, MainProcess.WHITE_BUTTON_GPIO,
                           self.rotB, MainProcess.ROTARY_SWITCH_GPIO)

        # Init the rest
        self.sb: SoundBank = SoundBank(mediaFolderPath)
        self.sb.printContent()
        self._timeCounter: int = MainProcess.COUNTER_START
        self._currentCategory: SoundCategory = self.sb.getSoundCategory(0)
        self._currentSound: Sound = self._currentCategory.getRandomSound()
        self._timer: Timer = Timer(0, self._doNothing)
        self._soundNext: Sound = Sound(SPECIALS_PATH, "nm.mp3")
        self._categoryNext: Sound = Sound(SPECIALS_PATH, "scratchSong.mp3")
        self._soundStart: Sound = Sound(SPECIALS_PATH, "ringtone-old_phone.mp3")
        self._endSc: Sound = Sound(SPECIALS_PATH, "nnms.mp3")
        self._soundStart.play()
        self._finishCategory: bool = False

    def _doNothing(self):
        """
        Does nothing. Exists so that we can have a Timer initialized during the constructor
        """
        return

    def run(self):
        """
        Body of the main()

        Watchs buttons states and do stuff accordingly
        """
        print('Start of  of MainProcess.run()')
        while True:
            if self.blueB.is_pressed:
                print("Blue Button pressed !")
                self.handleRepeat()
                while self.blueB.is_pressed:
                    pass
                print("Blue Button released !")
            if self.whiteB.is_pressed:
                print("White Button pressed !")
                self.handleNextSound()
                while self.whiteB.is_pressed:
                    pass
                print("White Button released !")
            if self.rotB.is_pressed:
                print("Rot Button pressed ! (_timeCounter = {})".format(self._timeCounter))
                self.handleCategorySwitch()
                while self.rotB.is_pressed:
                    pass
                print("Rot Button released ! (_timeCounter = {})".format(self._timeCounter))

    def handleRepeat(self):
        # First stop any playing sound
        Player.stopAllPlay()
        if self._finishCategory is True:  # play endOfCategory sound if we got the end of the category
            threadedExecution(self._endSc.play)
        else:
            threadedExecution(self._currentSound.play)

    def handleNextSound(self):
        Player.stopAllPlay()
        if self._finishCategory is True:  # play endOfCategory sound if we got the end of the category
            threadedExecution(self._endSc.play)
        else:  # else pick a new sound
            self._currentSound = self._currentCategory.getNextSound()
            if self._currentSound is None:  # if new sound is None, this is the end of the current soundCategory
                self._finishCategory = True
                threadedExecution(self._endSc.play)
            else:
                threadedExecution(self._soundNext.play)

    def announceNewCategorySound(self):
        """
        Plays a song to announce that a new category has been selected. Playes the category's it there is one, or
        a default sound if not.
        """
        if not self._currentCategory.playIntroSound():
            self._categoryNext.play()

    def changeCurrentCategory(self, index: int):
        """
        Changes of current category

        Parameters:
            index (int): index of the category in the MediaBank's category list.
        """
        print("Given index is {}".format(index))
        if index == 9:
            index = 0
            print("index changed from 9 to 0")

        sc: SoundCategory = self.sb.getSoundCategory(index)
        if sc is not None:
            print("changeCurrentCategory: New sound category is {} with index {}".format(sc.getName(), index))
            self._currentCategory = sc
            self._finishCategory = False
            self._currentCategory.shuffle()
            self.announceNewCategorySound()

    def timerRoutine(self):
        """
        Routine executed by the timer in charge of rotary switch's clicks
        """
        print("timerRoutine(): (_timeCounter = {})".format(self._timeCounter))
        if not isRpi():
            self._timeCounter *= 2

        self.changeCurrentCategory(int((self._timeCounter / 2) - 1))
        self._currentSound = self._currentCategory.getNextSound()
        self._timeCounter = MainProcess.COUNTER_START

    def handleCategorySwitch(self):
        """
        Method in charge of handling rotary switch's clicks counting
        """
        Player.stopAllPlay()
        self._timer.cancel()
        self._timeCounter += 1
        self._timer = Timer(MainProcess.TIMER_TIMEOUT_SECONDS, self.timerRoutine)
        self._timer.start()


if __name__ == "__main__":
    MainProcess(sys.argv[1]).run()
