import os
import subprocess

import simpleaudio as sa


class AudioFormat:
    def __init__(self, name: str):
        self.name = name

    def isFormatSupported(self, formatName: str) -> bool:
        return formatName == self.name

    def play(self, filepath: str): pass

    def stopPlaying(self): pass


class Mp3Format(AudioFormat):
    _playerName: str = "mpg123"

    def __init__(self):
        AudioFormat.__init__(self, ".mp3")
        self._process = subprocess.Popen(["false"])

    def play(self, filepath):
        # -q asks mpg123 to not print header in cout
        self._process = subprocess.Popen([Mp3Format._playerName, "-q", filepath])

    def stopPlaying(self):
        self._process.terminate()


class WavFormat(AudioFormat):
    def __init__(self):
        AudioFormat.__init__(self, ".wav")
        self.play_obj = sa.PlayObject(0)

    def play(self, filepath):
        wave_obj = sa.WaveObject.from_wave_file(filepath)
        self.play_obj = wave_obj.play()
        self.play_obj.wait_done()

    def stopPlaying(self):
        self.play_obj.stop()
        self.play_obj.wait_done()


class Player:
    _supportedFormats = [Mp3Format(), WavFormat()]

    @staticmethod
    def playFile(filepath: str):
        filename, file_extension = os.path.splitext(filepath)
        for af in Player._supportedFormats:
            if af.isFormatSupported(file_extension):
                af.play(filepath)
                break

    @staticmethod
    def stopAllPlay():
        for af in Player._supportedFormats:
            af.stopPlaying()
