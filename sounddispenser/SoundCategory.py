import os
from random import randrange, shuffle

from Sound import Sound


class SoundCategory:
    """
     Object representation of a SoundCategory (Ex: Samuel Lee Jackson, Movies, Video Games, Pop Culture, Haddock...)
     A Sound category is actually a folder containing audio files.
     Thus, one instance of this class stands for one audio file folder.
     Attributes:
         _sounds (list[Sound]): List of sounds that form that category
         _path (str): The path that lead to the category. Can be relative or absolute.
         _name (str): The name of the actual folder, and the category.
         _audioName: (Sound): Sound played when this category is selected
     """

    introFileName: str = "__intro__"

    def __init__(self, path: str, name: str):
        """
        SoundCategory's constructor

        Parameters:
            path (str): The path that lead to the category. Can be relative or absolute.
            name (str): The name of the actual folder, and the category.
        """
        self._sounds: list[Sound] = []
        self._path: str = path
        self._name: str = name
        self._audioName: Sound = None
        self._listIterator = iter(self._sounds)

    def addSound(self, toAdd: Sound):
        """
        Adds a sound to the internal list of sounds

        Parameters:
            toAdd (Sound): Sound instance to add to this category
        """
        self._sounds.append(toAdd)

    def removeSound(self, toRemove: Sound):
        """
        Removes a sound from the internal list of sounds

        Parameters:
            toRemove (Sound): Sound instance to remove from this category
        """
        self._sounds.remove(toRemove)

    def getFullPath(self) -> str:
        """
        Gives the path + name as a string. Ex: "/my/music/thisCategory"
        Returns:
            str: The category's path + name as a string.
        """
        return self._path + '/' + self._name

    def scanSounds(self):
        """
        Scans the folder represented by this category and adds every audio file found to the internal list of Sounds
        """
        fullpath: str = self.getFullPath()
        for root, dirs, files in os.walk(fullpath):
            for file in files:
                if file.startswith(SoundCategory.introFileName):
                    self._audioName = Sound(fullpath, file)
                else:
                    self.addSound(Sound(fullpath, file))

    def playIntroSound(self) -> bool:
        """
        If there is any, play this category's intro file

        Returns:
            True if intro file has been played, False otherwise
        """
        if self._audioName is not None:
            self._audioName.play()
            return True
        return False

    def printContent(self, indent: str):
        """
        Prints the full content of this category

        Parameters:
            indent (str): Text to add at the beginning of each new line. Generally an indentation
        """
        print(indent + self._name)
        for sound in self._sounds:
            print(indent + "    " + sound.getName())

    def playAll(self):
        """
        Plays all the available Sounds in this category
        """
        for sound in self._sounds:
            sound.play()

    def getRandomSound(self) -> Sound:
        """
        Returns a random Sound among all of the Sounds available in this category
        """
        return self._sounds[randrange(len(self._sounds))]

    def playRandomSound(self):
        """
        Plays a random Sound among all of the Sounds available in this category
        """
        self.getRandomSound().play()

    def getName(self) -> str:
        """
        Gives a copy of this Category's name
        Returns:
            str: The category's name.
        """
        return self._name

    def shuffle(self):
        """
        Shuffles the sound list
        """
        shuffle(self._sounds)
        self._listIterator = iter(self._sounds)

    def getNextSound(self):
        """
        Gives the next available song, or None
        Returns:
            str: The next available song, or None
        """
        try:
            return self._listIterator.__next__()
        except StopIteration:
            self._listIterator = iter(self._sounds)
            return None
