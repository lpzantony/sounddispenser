import os
from random import randrange

from CustomTools import CustomTools
from Sound import Sound
from SoundCategory import SoundCategory


class SoundBank:
    """
     Object representation of a Sound Bank.
     A Sound bank is actually a folder containing several folders that are known as SoundCategories.
     Each of those sub folders contains audio files.
     Thus, this class represents a folder containing folders containing audio files (mp3 or wav)
     Attributes:
         _soundCategories (dict[int, SoundCategory]): Dictionary of sound categories that form this SoundBank
         _path (str): The path that lead to this SoundBank. Can be relative or absolute.
         _name (str): The name of this SoundBank
     """

    def __init__(self, mediaFolderPath: str):
        """
        SoundBank's constructor

        Parameters:
            mediaFolderPath (str): The path that leads to the sound bank. Can be relative or absolute.
        """
        self._soundCategories: dict[int, SoundCategory] = {}
        self._path: str = mediaFolderPath
        self._name: str = CustomTools.path_leaf(self._path)
        self._scanMedia(self._path)

    def _scanMedia(self, mediaFolderPath):
        """
        Scans the folder represented by this sound bank and adds every SoundCategory
        found to the internal list of categories

        Parameters:
            mediaFolderPath (str): The path that leads to the sound categories. Can be relative or absolute.
        """
        index: int = 0
        for root, dirs, files in os.walk(mediaFolderPath):
            for folder in dirs:
                self._soundCategories[index] = SoundCategory(root, folder)
                self._soundCategories[index].scanSounds()
                index += 1

    def printContent(self):
        """
        Prints the full content of this Sound Bank
        """
        print("printing content of \"" + self._name + "\" located in : " + CustomTools.path_root(self._path))
        for idx, sc in self._soundCategories.items():
            sc.printContent("   {}: ".format(idx))

    def getSoundCategory(self, category):
        """
        Gets the full content of a category

        Parameters:
            categoryName (str or int): The name or index of the category. Should be the same than the file this category represents.
         Returns:
            SoundCategory: The corresponding SoundCategory, or None if couldn't be found
        """
        if type(category) is str:
            for sc in self._soundCategories.values():
                if category == sc.getName():
                    return sc
            return None
        if type(category) is int:
            if category < len(self._soundCategories.keys()):
                return self._soundCategories[category]
            return None

    def playSoundCategory(self, categoryName: str):
        """
        Plays the full content of a category

        Parameters:
            categoryName (str): The name of the category. Should be the same than the file this category represents.
        """
        print("playSoundCategory called for " + categoryName)
        sc = self.getSoundCategory(categoryName)
        if sc is not None:
            sc.playAll()

    def playSoundCategory(self, index: int):
        """
        Plays the full content of a category

        Parameters:
            index (int): index of the category.
        """
        print("playSoundCategory called for index {}".format(index))
        sc = self.getSoundCategory(index)
        if sc is not None:
            sc.playAll()

    def getRandomCategory(self) -> SoundCategory:
        """
        Gets a random Category among all the categories available in this SoundBank
        """
        return self._soundCategories[randrange(len(self._soundCategories))]

    def getRandomSound(self) -> Sound:
        """
        Gets a random Sound among all the categories available in this SoundBank
        """
        return self.getRandomCategory().getRandomSound()

    def playRandomCategory(self):
        """
        Plays the whole content of a random Category among all the categories available in this SoundBank
        """
        self.getRandomCategory().playAll()

    def playRandomSound(self, category=None):
        """
        Plays a random Sound among all the categories available in this SoundBank

        Parameters:
            category (str or int): Optional name or index of category if the caller wishes to pick a random Sound in a specific one.
        """
        if category is None:
            self.getRandomSound().play()
        elif type(category) is str:
            sc = self.getSoundCategory(category)
            if sc is not None:
                sc.playRandomSound()
        elif type(category) is int:
            sc = self.getSoundCategory(category)
            if sc is not None:
                sc.playRandomSound()
