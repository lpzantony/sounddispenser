import ntpath


class CustomTools:
    """
    Tool class made of static methods only
    """

    @staticmethod
    def path_leaf(path: str) -> str:
        """
        Gives the last element in a path, include the extension if there is any

        Returns:
            str: The last element in a path, include the extension if there is any
        """
        head, tail = ntpath.split(path)
        return tail or ntpath.basename(head)

    @staticmethod
    def path_root(path) -> str:
        """
        Gives all what precedes the last element in a path

        Returns:
            str:  All what precedes the last element in a path
        """
        head, tail = ntpath.split(path)
        return head
