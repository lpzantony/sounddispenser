from time import sleep

from gpiozero import Button
from gpiozero import Device
from gpiozero.pins.mock import MockFactory
from pynput import keyboard


class CustomButton:
    """
     Custom Object representation of a Button
     A Custom Button is simply a Button with a name and a GPIO number
     Attributes:
         name (str): name of the button, for dbug purpose only
         button (Button): The actual Button it represents
         gpio (int): The GPIO number to which this Button is connected
         gpioHandler : (???) used to toggle button
     """

    def __init__(self, name: str, button: Button, gpioIdx: int):
        """
        CustomButton's constructor

        Parameters:
            name (str): name of the button, for dbug purpose only
            button (Button): The actual Button it represents
            gpio (int): The GPIO number to which this Button is connected
        """
        self.name = name
        self.button: Button = button
        self.gpio: int = gpioIdx
        self.gpioHandler = Device.pin_factory.pin(gpioIdx)
        self.release()

    def press(self):
        """
        Perform a virtual press to the button. Takes into account inverted buttons.
        """
        if self.button._active_state is None:
            if self.button.pull_up is True:
                self.gpioHandler.drive_low()
            else:
                self.gpioHandler.drive_high()
        elif self.button._active_state is True:
            self.gpioHandler.drive_high()
        else:
            self.gpioHandler.drive_low()

    def release(self):
        """
        Perform a virtual release to the button. Takes into account inverted buttons.
        """
        if self.button._active_state is None:
            if self.button.pull_up is True:
                self.gpioHandler.drive_high()
            else:
                self.gpioHandler.drive_low()
        elif self.button._active_state is True:
            self.gpioHandler.drive_low()
        else:
            self.gpioHandler.drive_high()

    def click(self, clickCount: int = 1, clickDuration: float = 0.1):
        """
        Perform a virtual click (press then release) to the button. Takes into account inverted buttons.

        Parameters:
            clickCount (str): The count of clicks you want to perform
            clickDuration (int): duration of each step in seconds. Thus a click is 2*clickDuration long.
        """
        for x in range(clickCount):
            self.press()  # Drive the pin low (this is what would happen electrically when the button is pushed)
            sleep(clickDuration)  # give source some time to re-read the button state
            self.release()
            sleep(clickDuration)


# ======================================================================== #
# ======================================================================== #
# ======================================================================== #
# ======================================================================== #
# ======================================================================== #

class GpioEmulator:
    """
     Object representation of Rpi's GPIO. This class is used whenever the code is executed on a PC.

     Attributes:
         blueB (CustomButton): CustomButton instance for the blue button
         whiteB (CustomButton): CustomButton instance for the white button
         rotB (CustomButton): CustomButton instance for the rotary button
         listener : (???) keyboard listener
     """

    def __init__(self):
        """
        GpioEmulator's constructor
        """
        # Set the default pin factory to a mock factory
        Device.pin_factory = MockFactory()
        self.blueB: CustomButton = CustomButton("Dummy blueB", Button(2), 0)
        self.whiteB: CustomButton = CustomButton("Dummy whiteB", Button(3), 0)
        self.rotB: CustomButton = CustomButton("Dummy rotB", Button(4), 0)
        self.listener = keyboard.Listener(on_press=self.on_press, on_release=self.on_release)

    def start(self,
              blueB: Button, blueB_gpio: int,
              whiteB: Button, whiteB_gpio: int,
              rotB: Button, rotB_gpio: int):
        """
        Starts the GPIO emulator

        Parameters:
            blueB (CustomButton): CustomButton instance for the blue button
            blueB_gpio (int): number of the GPIO to which the blue button is connected
            whiteB (CustomButton): CustomButton instance for the white button
            whiteB_gpio (int): number of the GPIO to which the white button is connected
            rotB (CustomButton): CustomButton instance for the rotary button
            rotB_gpio (int): number of the GPIO to which the white button is connected
        """
        self.blueB: CustomButton = CustomButton("blueB", blueB, blueB_gpio)
        self.whiteB: CustomButton = CustomButton("whiteB", whiteB, whiteB_gpio)
        self.rotB: CustomButton = CustomButton("rotB", rotB, rotB_gpio)
        print('End of GpioEmulator.start()')
        self.listener.start()

    def stop(self):
        """
        Stops the GPIO emulator
        """
        self.listener.stop()
        self.listener.join()

    def on_press(self, key):
        """
        Callback called by the listener any time a key is pressed

        Parameters:
            key (???): The key that has been pressed
        """
        try:
            if key == keyboard.Key.enter:
                print('')
                return
            print('\nKey {0} pressed'.format(key.char))
            if key.char == '1':
                self.rotB.click(clickCount=1)  # movies
            elif key.char == '2':
                self.rotB.click(clickCount=2)  # popculture
            elif key.char == '3':
                self.rotB.click(clickCount=3)  # haddock
            elif key.char == '4':
                self.rotB.click(clickCount=4)  # videoGames
            elif key.char == '5':
                self.rotB.click(clickCount=5)  # slj
            elif key.char == '9':
                self.rotB.click(clickCount=10)  # movies
            elif key.char == 'w':
                self.whiteB.click()  # next
            elif key.char == 'b':
                self.blueB.click()  # repeat
        except AttributeError:
            print('special key {0} pressed'.format(key))

    def on_release(self, key):
        """
        Callback called by the listener any time a key is released

        Parameters:
            key (???): The key that has been released
        """
        if key == keyboard.Key.enter:
            return
        print('{0} released'.format(key))
        if key == keyboard.Key.esc:
            # Stop listener
            return False
